<?php
    include('navbar_user.php');
?>

        <title>Edit & Update</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">


        <h2>Password Update</h2>

        <?php 
            if(isset($_GET['id'])) {
                $user_id = $_GET['id'];

                $query = "SELECT * FROM users WHERE id=:u_id";
                $statement = $conn->prepare($query);
                $data = [ 'u_id' => $user_id];
                $statement->execute($data);

                $result = $statement->fetch(PDO::FETCH_OBJ);

            }
        

        ?>

        
        <form action="code/code_u_update_password.php" method="POST">
            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
            <label>Old Password</label>
            <input type="text" name="o_password"><br>
            <label>New Password</label>
            <input type="text" name="n_password"><br>
            <label>Confirm New Password</label>
            <input type="text" name="c_password"><br>
            <!-- <input type="password" name="password">
            <label>Confirm Password:</label>
            <input type="password" name="c_password"><br> -->
            <input type="submit" name="p_update_btn" value="Change Password">
        </form>
        

    </body>
</html>
