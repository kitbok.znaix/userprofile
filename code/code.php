<?php
    include('security.php');

    // Insert DATA
    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $c_password = $_POST['c_password'];
        $role_no = $_POST['role_no'];

        //check password Match
        if ($password != $c_password) {
            // Passwords don't match, display error message
            $_SESSION['error'] = 'Passwords do not match.';
            header('Location: ../login.php');
            exit;
        }

        // hash password
        $h_password = password_hash($password, PASSWORD_BCRYPT);

        $query 	= "INSERT INTO users (name, phone, email, password, role_no) VALUES (:name, :phone, :email, :h_password, :role_no)";
        $query_run = $conn->prepare($query);

        $data = [
            ':name' => $name,
            ':phone' => $phone,
            ':email' => $email,
            ':h_password' => $h_password,
            ':role_no' => $role_no,
        ];

        $query_execute = $query_run->execute($data);

        if($query_execute) {
            $_SESSION['message'] = "Inserted Successfully";
            header('location: ../login.php');
            exit(0);
        } else {
            $_SESSION['message'] = "Not Inserted";
            header('location: ../login.php');
            exit(0);
        }
    }
?>