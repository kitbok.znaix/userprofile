<?php 
    session_start();
    include('config.php');

    if(isset($_POST['a_update_btn'])) {
        $user_id = $_POST['user_id'];
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        $role = $_POST['role'];

        try {
            $query = "UPDATE users SET name=:name, phone=:phone, role_no=:role WHERE id=:u_id";
            $statement = $conn->prepare($query);
            
            $data = [
                ':name' => $name,
                ':phone' => $phone,
                ':role' => $role,
                ':u_id' => $user_id
            ];
            $query_execute = $statement->execute($data);

            if($query_execute) {
                $_SESSION['message'] = "updated Successfully";
                header('location: ../a_dashboard.php');
                exit(0);
            } else {
                $_SESSION['message'] = "Not Updated";
                header('location: ../a_dashboard.php');
                exit(0);
                }

        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
?>