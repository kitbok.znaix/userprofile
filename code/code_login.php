<?php
include('security.php');

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $pass = $_POST['password'];
    // $hash = password_hash($pass, PASSWORD_DEFAULT);
    try {
        // include_once 'config.php';
        $select = $conn->prepare("SELECT * FROM users WHERE email=:email");
        $select->bindParam(':email', $email);
        $select->execute();
        $user = $select->fetch(PDO::FETCH_ASSOC);
        if ($user && password_verify($pass, $user['password'])) {
            $_SESSION['id'] = $user['id'];
            // $_SESSION['name'] = $user['name'];
            // $_SESSION['phone'] = $user['phone'];
            // $_SESSION['email'] = $user['email'];
            //switch case
            if ($user['role_no'] == 1) {
                header("Location: ../a_dashboard.php");
                exit;
            } elseif ($user['role_no'] == 2) {
                header("Location: ../u_dashboard.php");
                exit;
            }
        } else {
            $_SESSION['message'] = "Incorrect email or password!!";
            header("Location: ../login.php");
            exit;
        }
    } catch(PDOException $e) {
        $_SESSION['message'] = "Error logging in: " . $e->getMessage();
        header("Location: ../login.php");
        exit;
    }
}
?>
