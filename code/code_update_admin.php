<?php
include('security.php');

if(isset($_POST['submit_edit_id'])) {
    $user_id = $_POST['submit_edit_id'];
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $role_name = $_POST['role_name'];

    try {
        $query = "SELECT * FROM users WHERE id=:u_id";
        $statement = $conn->prepare($query);
        
        $data = [
            ':name' => $name,
            ':phone' => $phone,
            ':role_name' => $role_name,
            ':u_id' => $user_id
        ];
        $query_execute = $statement->execute($data);

        if($query_execute) {
            $_SESSION['message'] = "updated Successfully";
            header('location: ../a_dashboard.php');
            exit(0);
        } else {
            $_SESSION['message'] = "Not Updated";
            header('location: ../a_dashboard.php');
            exit(0);
            }

    } catch(PDOException $e) {
        echo $e->getMessage();
    }

}
?>