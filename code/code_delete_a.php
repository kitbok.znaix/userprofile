<?php
    include('security.php');

    if(isset($_POST['delete_a'])) {
        $user_id = $_POST['delete_u'];

        try{
            $query = "DELETE FROM users WHERE id=:u_id";
            $statement = $conn->prepare($query);
            $data = [
                'u_id' => $user_id
            ];
            $query_execute = $statement->execute($data);

            if($query_execute) {
                $_SESSION['message'] = "Delete Successfully";
                header('location: ../login.php');
                exit(0);
            } else {
                $_SESSION['message'] = "Delete Failed";
                header('location: ../a_dashboard.php');
                exit(0);
            }


        } catch(PDOException $e) {
            echo $e->getMessage();
        }

    }



?>