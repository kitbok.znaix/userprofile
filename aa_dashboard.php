<?php
    // session_id($a_user);
    session_start();
    include('code/config.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin</title>
        <link rel="stylesheet" href="admin_styles.css">
        <!-- <link rel="stylesheet" href="https://classless.de/classless.css"> -->
    </head>
    <body>
        <h2>Registration form</h2>

        <h1>Welcome <?= $_SESSION['name']; ?></h1>
        <div class="contain">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Phone No</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>

                <?php
                    $query = "SELECT u.id, u.name, u.phone, u.email, r.role_name FROM role r INNER JOIN users u ON u.role_no = r.id;";  //SELECT * FROM users
                    $statement = $conn->prepare($query);
                    $statement->execute();

                    
                    $result = $statement->fetchAll(PDO::FETCH_OBJ);
                                    
                    if($result) { // check record
                    ?>
                        <p> Current Record</p>
                    <?php
                        foreach($result as $row) {

                            ?>

                        <tr>
                            <td><?= $row->name; ?></td>
                            <td><?= $row->phone; ?></td>
                            <td><?= $row->email; ?></td>
                            <td><?= $row->role_name; ?></td>
                        </tr>
                        
                        <?php
                        }
                    } else {
                        ?>
                        <p> NO Record Found</p>
                    <?php
                     }
                    ?>

            </table>
        </div>




    </body>
</html>
