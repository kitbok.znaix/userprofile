<?php 
    include('navbar_admin.php');
?>

  <title>ADMIN</title>
  <link rel="stylesheet" href="style/dashboard_styles.css"/>


  <!-- <button id="showPopupBtn">Open Form</button> -->
<?php 
    $user_id = $_SESSION['id'];

    $query = "SELECT * FROM users WHERE id=:u_id";
    $statement = $conn->prepare($query);
    $data = [ 'u_id' => $user_id];
    $statement->execute($data);

     $result = $statement->fetch(PDO::FETCH_OBJ);

?>

<div id="popupForm" class="container">
  <div class="registration form">
      <header>Update</header>
      <form action="code/code_update_a.php?id=<?= $_SESSION['id']; ?>" method="post">
      <input type="hidden" name="user_id" value="<?php echo $result->id; ?>">
        <input type="text" placeholder="Enter your name" required name="name" value="<?php echo $result->name; ?>">
        <input type="number" placeholder="Enter your phone number" name="phone" value="<?php echo $result->phone; ?>">
        <input type="email" placeholder="Enter your email" required name="email" value="<?php echo $result->email; ?>">
          <!-- button -->
        <div class="btn-container">
          <input type="submit" class="button button-submit" value="Update" id="update" name="u_update_btn">
          <input type="button" class="button button-cancel" value="Cancel" id="cancel">
        </div>

      </form>
      <div class="signup">
        <a href="u_update_password.php">Change Password</a>
      </div>
    </div>
</div>





  <!-- USER DASHBOARD -->

<?php
        $id = $_SESSION['id']; // replace with the actual id you want to fetch
        $query = ("SELECT * FROM users WHERE id = :id");
        $stmt = $conn->prepare($query); //prepare the query for execution, this way we avoid dynamic SQL injection if possible.
        $stmt->execute(['id' => $id]);
        $user = $stmt->fetch(PDO::FETCH_OBJ);

        if ($user) {
      ?>

<div class="header">
      <h1>Welcome <?= $user->name; ?></h1>
    </div>
    <div class="row">
      <div class="column">
        <h2>USER</h2>
        <ul>
          <li>Phone No: &nbsp<?= $user->phone ?></li>
          <li>Email Address: &nbsp<?= $user->email; ?></a></li><br>
        </ul>
        <?php
        } else {
          echo "user Not found";
        }
        ?>
        <div class="horizon">
          <div class="center">
          <button type="submit" id="edit">Edit</button>
            <form action="code/code_delete_a.php" method="post">
              <button type="submit" name="delete_a" value="<?= $user->id; ?>">Delete</button>
              <button type="submit" name="delete_u"><a href="display_other.php?name=<?= $user->name; ?>">View Other's</a></button>
            </form>
          </div>
        </div>
      </div>


      <script src="js/update_popup.js"></script>

<?php if(isset($_SESSION['message'])) : ?>
            <h5><?= $_SESSION['message'] ?></h5>
            <?php
                unset($_SESSION['message']);
                endif; 
?>


</body>
</html>