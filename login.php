<?php 
    session_start();
    include('code/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>login</title>
  <link rel="stylesheet" href="stylelogin.css">
</head>
<body>
  <div class="container">
    <input type="checkbox" id="check">
    <div class="login form">

      <?php if(isset($_SESSION['message'])) : ?>
                <h5><?= $_SESSION['message'] ?></h5>
                <?php
                    unset($_SESSION['message']);
                    endif; 
      ?>

      <header>Login</header>
      <form action="code/code_login.php" method="post">
        <input type="text" placeholder="Enter your email" name="email" required>
        <input type="password" placeholder="Enter your password" name="password" required>
        <input type="submit" class="button" value="Login" name="login">
      </form>
      <div class="signup">
        <span class="signup">Don't have an account?
         <label for="check">Signup</label>
        </span>
      </div>
    </div>
    <div class="registration form">
      <header>Signup</header>
      <form action="code/code.php" method="post">
        <input type="text" placeholder="Enter your name" required name="name" >
        <input type="number" placeholder="Enter your phone number" required name="phone">
        <input type="email" placeholder="Enter your email" required name="email">
        <input type="password" placeholder="Create a password" name="password" required>
        <input type="password" placeholder="Confirm your password" name="c_password" required>
        <input type="hidden" name="role_no" value="2">
        <input type="submit" class="button" value="Signup" name="submit">
      </form>
      <div class="signup">
        <span class="signup">Already have an account?
         <label for="check">Login</label>
        </span>
      </div>
    </div>
  </div>
</body>
</html>
