const edit = document.getElementById('edit');
const popupForm = document.getElementById('popupForm');
const update = document.getElementById('update');
const cancel = document.getElementById('cancel');

// Show the popup form when the button is clicked
edit.addEventListener('click', function() {
    popupForm.style.display = 'block';
  });

// Hide the popup form when the cancel button is clicked
cancel.addEventListener('click', function() {
    popupForm.style.display = 'none';
  });

